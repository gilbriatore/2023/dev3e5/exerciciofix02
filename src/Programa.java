
import java.util.Scanner;

public class Programa {
	
	public static void main(String[] args) {
		
		
		/*		 
		 	Transformar o programa do exercício 1 para um sistema que permita ler a
			entrada de dados pelo usuário em um vetor durante execução;
			Permitir que o usuário informe, primeiro , os dados de 5 (cinco) alunos e depois
			de capturados os dados, imprimir o relatório final com todos os dados:
			Matrícula: xxxxx
			Nome:xxxxx xxxxx
			Aprovado: (x) Sim ( ) Não
			Nota final: 	xxxxx		 		 
		 */
		
		Scanner leitor = new Scanner(System.in);
		
		int qtde = 2;
		
		String[] matriculas = new String[qtde];
		String[] nomes = new String[qtde];
		int[] notas1 = new int[qtde];
		int[] notas2 = new int[qtde];
		
		for(int i = 0; i < qtde; i++) {
		
			System.out.println("Informe a matrícula: ");
			matriculas[i] = leitor.nextLine();
			
			System.out.println("Informe o nome:");
			nomes[i] = leitor.nextLine();
			
			System.out.println("Informe a nota 1:");
			notas1[i] = leitor.nextInt();
			
			System.out.println("Informe a nota 2");
			notas2[i] = leitor.nextInt();
			
			//Forçar a quebra de linha
			leitor.nextLine();			
		}		
		
		
		int contador = 0;
		while (contador < qtde) {
			
			System.out.println("Nome: " + nomes[contador]);
			System.out.println("Matrícula: " + matriculas[contador]);
			
			int notaFinal = (notas1[contador] + notas2[contador]) / 2;
			if (notaFinal >= 6) {
				System.out.println("Aprovado: (x) Sim ( ) Não");
			} else {
				System.out.println("Aprovado: ( ) Sim (x) Não");
			}
			System.out.println("Notal final: " + notaFinal);			
			
			contador++;
		}
				
		leitor.close();
		
	}

}
